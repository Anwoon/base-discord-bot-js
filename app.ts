import { Client } from 'discord.js';
import { load } from 'yamljs';
import { Dispatcher } from './src/Actions/dispatcher'

class App {
  private app: Client
  private config: any
  private dispatcher: Dispatcher

  constructor() {
    this.app = new Client()
    this.config = load('settings.yml')
    this.dispatcher = new Dispatcher()
  }

  init() {
    this.app.on('ready', () => {
      console.log(`Logged in as ${this.app.user.tag}`)
    })

    this.app.on('message', msg => {
      var args = msg.content.split(/ +/g);
      const cmd = args.shift().toLowerCase();
      const prefix = this.config.settings.prefix;

      if (msg.author.bot) return;

      if (cmd) {
        this.dispatcher.run(cmd, args, msg, prefix);
      }
    });

    this.app.login(this.config.settings.token);
  }
}

const app = new App();
app.init();